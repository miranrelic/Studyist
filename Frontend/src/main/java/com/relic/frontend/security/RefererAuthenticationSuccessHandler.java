package com.relic.frontend.security;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.web.util.WebUtils;

import java.io.IOException;
import java.util.Optional;

public class RefererAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Value("${frontend.url}")
    String frontendUrl;

    public RefererAuthenticationSuccessHandler() {
        super();
//        setUseReferer(true);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication) throws IOException {

        Optional<Cookie> redirectUri = Optional.ofNullable(WebUtils.getCookie(request, "redirectUri"));

        if (redirectUri.isPresent()) {
            response.sendRedirect(redirectUri.get().getValue());
        } else response.sendRedirect(frontendUrl);

    }
}
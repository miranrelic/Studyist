package com.relic.frontend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ApiUser {
    private String userId;
    private String keycloakUserId;

    private String firstName;
    private String lastName;
    private String email;

    public ApiUser(String keycloakUserId, String firstName, String lastName, String email) {
        this.keycloakUserId = keycloakUserId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

}

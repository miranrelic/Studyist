package com.relic.frontend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Subject {

    private String subjectId;
    private String subjectName;

    private List<SubjectType> subjectTypes;
}

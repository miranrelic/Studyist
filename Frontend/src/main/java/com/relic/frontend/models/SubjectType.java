package com.relic.frontend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectType {
    private String subjectTypeId;
    private String typeName;
    private String professorId;
    private Double mandatoryAttendancePercentage;
    private Double totalSubjectTypeHours;
    private Double attendanceHourValue;
    private Double currentAttendancePercentage;
    private Double currentAttendanceCount;
    private Double attendanceCountLeft;

}

package com.relic.frontend.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
public class User {
    String userID;
    String firstName;
    String lastName;
    String email;
    List<String> roles;

    public User(OAuth2AuthenticationToken token) {
        OAuth2User auth2User = token.getPrincipal();
        this.roles = auth2User.getAttribute("realm_roles");
        this.email = auth2User.getAttribute("preferred_username");
        this.firstName = auth2User.getAttribute("given_name");
        this.lastName = auth2User.getAttribute("family_name");
        this.userID = auth2User.getName();
    }

    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}

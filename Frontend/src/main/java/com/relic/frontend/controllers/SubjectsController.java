package com.relic.frontend.controllers;

import com.relic.frontend.models.Exception;
import com.relic.frontend.models.PaginatedResponse;
import com.relic.frontend.models.Subject;
import com.relic.frontend.models.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Objects;

@PreAuthorize("isAuthenticated()")
@Controller
@RequestMapping("/studies/{studyID}/subjects")
public class SubjectsController {

    @Value("${api.url}")
    String apiUrl;

    @GetMapping
    public String index(Model model, @PathVariable String studyID,
                        RedirectAttributes redirectAttrs,
                        OAuth2AuthenticationToken authenticationToken
    ) {
        User user = new User(authenticationToken);
        model.addAttribute("user", user);
        String uri = apiUrl + "/users/" + user.getUserID() + "/studies/" + studyID + "/subjects";

        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<PaginatedResponse<Subject>> result = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );
            List<Subject> subjects = Objects.requireNonNull(result.getBody()).getContent();
            model.addAttribute("subjects", subjects);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/colleges";
        }
        model.addAttribute("studyID", studyID);
        model.addAttribute("pageTitle", "Subjects");
        return "subjects";
    }


    @GetMapping("/new")
    public String newSubject(Model model, @PathVariable String studyID) {

        Subject subject = new Subject();
        String uri = "/studies/" + studyID + "/subjects";

        model.addAttribute("subject", subject);
        model.addAttribute("uri", uri);
        model.addAttribute("method", "post");

        return "subjects_form";
    }

    //
    @PostMapping
    public RedirectView addSubject(@ModelAttribute Subject subject,
                                   @PathVariable String studyID,
                                   RedirectAttributes redirectAttrs,
                                   OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = "/studies/" + studyID + "/subjects";

        try {
            restTemplate.exchange(
                    apiUrl + "/users/" + user.getUserID() + uri,
                    HttpMethod.POST,
                    new HttpEntity<Subject>(subject),
                    Void.class
            );
            String message = "Study " + subject.getSubjectName() + " added.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }
        RedirectView rv = new RedirectView();
        rv.setUrl(uri);
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;

    }

    //
    @DeleteMapping("/{subjectID}")
    public RedirectView deleteSubject(@PathVariable String studyID,
                                      @PathVariable String subjectID,
                                      RedirectAttributes redirectAttrs,
                                      OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = "/studies/" + studyID + "/subjects";

        Subject subject = restTemplate.exchange(
                apiUrl + "/users/" + user.getUserID() + uri + "/" + subjectID,
                HttpMethod.GET,
                null,
                Subject.class
        ).getBody();
        assert subject != null;

        try {
            restTemplate.exchange(
                    apiUrl + "/users/" + user.getUserID() + uri + "/" + subjectID,
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
            String message = "Subject " + subject.getSubjectName() + " deleted.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl(uri);
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }

    @GetMapping("/edit/{subjectID}")
    public String editSubject(Model model,
                              RedirectAttributes redirectAttrs,
                              @PathVariable String studyID,
                              @PathVariable String subjectID,
                              OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        Subject subject;
        String uri = "/studies/" + studyID + "/subjects";

        try {
            String apiUri = apiUrl + "/users/" + user.getUserID() + uri + "/" + subjectID;
            subject = restTemplate.exchange(
                    apiUri,
                    HttpMethod.GET,
                    null,
                    Subject.class
            ).getBody();
            model.addAttribute("subject", subject);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/" + uri;
        }

        model.addAttribute("studyID", studyID);
        model.addAttribute("uri", uri + "/" + subjectID);
        model.addAttribute("method", "put");

        return "subjects_form";
    }

    //
    @RequestMapping(value = "/{subjectID}", method = {RequestMethod.POST, RequestMethod.PUT})
    public RedirectView updateProfessor(@ModelAttribute Subject subject,
                                        @PathVariable String studyID,
                                        @PathVariable String subjectID,
                                        RedirectAttributes redirectAttrs,
                                        OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = "/studies/" + studyID + "/subjects";

        try {
            restTemplate.exchange(
                    apiUrl + "/users/" + user.getUserID() + uri + "/" + subjectID,
                    HttpMethod.PUT,
                    new HttpEntity<Subject>(subject),
                    Void.class
            );
            String message = "Subject " + subject.getSubjectName() + " updated.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl(uri);
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }
}

package com.relic.frontend.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.relic.frontend.models.Exception;
import com.relic.frontend.models.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Objects;

import static java.lang.Math.floor;

@PreAuthorize("isAuthenticated()")
@Controller
@RequestMapping("/studies/{studyID}/subjects/{subjectID}")
public class SubjectTypeController {

    @Value("${api.url}")
    String apiUrl;

    @GetMapping
    public String getSubject(Model model,
                             @PathVariable String studyID,
                             @PathVariable String subjectID,
                             RedirectAttributes redirectAttrs,
                             OAuth2AuthenticationToken authenticationToken
    ) {
        User user = new User(authenticationToken);
        model.addAttribute("user", user);
        String uri = apiUrl + "/users/" + user.getUserID() + "/studies/" + studyID + "/subjects/" + subjectID;
        Subject subject;
        try {
            RestTemplate restTemplate = new RestTemplate();
            subject = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    null,
                    Subject.class
            ).getBody();
            assert subject != null;
            model.addAttribute("subjectTypes", subject.getSubjectTypes());
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/studies/" + studyID + "/subjects";
        }
        model.addAttribute("studyID", studyID);
        model.addAttribute("subjectID", subjectID);
        model.addAttribute("pageTitle", subject.getSubjectName());
        return "subject_types";
    }

    @GetMapping("/new-type")
    public String newType(Model model, @PathVariable String studyID, @PathVariable String subjectID,
                          OAuth2AuthenticationToken authenticationToken) {

        SubjectType subjectType = new SubjectType();
        String submitUri = "/studies/" + studyID + "/subjects/" + subjectID + "/new-type";
        String cancelUri = "/studies/" + studyID + "/subjects/" + subjectID;

        User user = new User(authenticationToken);
        model.addAttribute("user", user);
        String apiUri = apiUrl + "/users/" + user.getUserID() + "/professors";

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<PaginatedResponse<Professor>> result = restTemplate.exchange(
                apiUri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        List<Professor> professors = Objects.requireNonNull(result.getBody()).getContent();

        model.addAttribute("subjectType", subjectType);
        model.addAttribute("submitUri", submitUri);
        model.addAttribute("cancelUri", cancelUri);
        model.addAttribute("professors", professors);
        model.addAttribute("method", "post");

        return "subject_types_form";
    }

    @GetMapping("/new-professor")
    public String newProfessor(Model model, @PathVariable String studyID, @PathVariable String subjectID) {

        Professor professor = new Professor();
        String submitUri = "/studies/" + studyID + "/subjects/" + subjectID + "/new-professor";
        String cancelUri = "/studies/" + studyID + "/subjects/" + subjectID + "/new-type";


        model.addAttribute("professor", professor);
        model.addAttribute("submitUri", submitUri);
        model.addAttribute("cancelUri", cancelUri);
        model.addAttribute("hxTarget", "closest .card");
        model.addAttribute("hxSwap", "outerHTML");
        model.addAttribute("method", "post");

        return "professor_form";
    }

    @GetMapping("/edit/{typeID}")
    public String editType(
            Model model,
            RedirectAttributes redirectAttrs,
            @PathVariable String studyID,
            @PathVariable String subjectID,
            @PathVariable String typeID,
            OAuth2AuthenticationToken authenticationToken
    ) {
        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();

        String submitUri = "/studies/" + studyID + "/subjects/" + subjectID + "/edit/" + typeID;
        String cancelUri = "/studies/" + studyID + "/subjects/" + subjectID;
        String apiProfUri = apiUrl + "/users/" + user.getUserID() + "/professors";
        String apiTypeUri = apiUrl + "/users/" + user.getUserID() + "/subjects/" + subjectID + "/subjectTypes/" + typeID;
        SubjectType subjectType;

        ResponseEntity<PaginatedResponse<Professor>> professorsResponse = restTemplate.exchange(
                apiProfUri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        List<Professor> professors = Objects.requireNonNull(professorsResponse.getBody()).getContent();
        try {
            subjectType = restTemplate.exchange(
                    apiTypeUri,
                    HttpMethod.GET,
                    null,
                    SubjectType.class
            ).getBody();
            assert subjectType != null;
            double mandatoryPercentage = Double.parseDouble(String.format("%.0f", floor(subjectType.getMandatoryAttendancePercentage() * 100)));
            subjectType.setMandatoryAttendancePercentage(mandatoryPercentage);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:" + cancelUri;
        }

        model.addAttribute("subjectType", subjectType);
        model.addAttribute("submitUri", submitUri);
        model.addAttribute("cancelUri", cancelUri);
        model.addAttribute("professors", professors);
        model.addAttribute("method", "put");

        return "subject_types_form";
    }

    @PostMapping("/new-type")
    public String addType(
            @ModelAttribute SubjectType subjectType,
            RedirectAttributes redirectAttrs,
            OAuth2AuthenticationToken authenticationToken,
            @PathVariable String studyID,
            @PathVariable String subjectID
    ) throws JsonProcessingException {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String apiUri = apiUrl + "/users/" + user.getUserID() + "/subjects/" + subjectID + "/subjectTypes";
        String uri = "/studies/" + studyID + "/subjects/" + subjectID;

        try {
            restTemplate.exchange(
                    apiUri,
                    HttpMethod.POST,
                    new HttpEntity<SubjectType>(subjectType),
                    Void.class
            );
            String message = "Subject type " + subjectType.getTypeName() + " added.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        return "redirect:" + uri;
    }

    @PostMapping("/new-professor")
    public String addProfessor(
            @ModelAttribute Professor professor,
            RedirectAttributes redirectAttrs,
            OAuth2AuthenticationToken authenticationToken,
            @PathVariable String studyID,
            @PathVariable String subjectID
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String apiUri = apiUrl + "/users/" + user.getUserID() + "/professors";
        String uri = "/studies/" + studyID + "/subjects/" + subjectID;

        try {
            restTemplate.exchange(
                    apiUri,
                    HttpMethod.POST,
                    new HttpEntity<Professor>(professor),
                    Void.class
            );
            String message = "Professor " + professor.getFirstName() + " " + professor.getLastName() + " added.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        return "redirect:" + uri;
    }

    @RequestMapping(value = "/edit/{typeID}", method = {RequestMethod.POST, RequestMethod.PUT})
    public RedirectView updateType(@ModelAttribute SubjectType subjectType,
                                   @PathVariable String studyID,
                                   @PathVariable String subjectID,
                                   @PathVariable String typeID,
                                   RedirectAttributes redirectAttrs,
                                   OAuth2AuthenticationToken authenticationToken
    ) {
        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String apiUri = apiUrl + "/users/" + user.getUserID() + "/subjects/" + subjectID + "/subjectTypes/" + typeID;
        ;
        String uri = "/studies/" + studyID + "/subjects/" + subjectID;

        try {
            restTemplate.exchange(
                    apiUri,
                    HttpMethod.PUT,
                    new HttpEntity<SubjectType>(subjectType),
                    Void.class
            );
            String message = "Subject type " + subjectType.getTypeName() + " updated.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl(uri);
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }

    @PostMapping("/decrementAttendance/{typeID}")
    public String decrementAttendance(
            RedirectAttributes redirectAttrs,
            OAuth2AuthenticationToken authenticationToken,
            @PathVariable String studyID,
            @PathVariable String subjectID,
            @PathVariable String typeID
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String apiUri = apiUrl + "/users/" + user.getUserID() + "/subjects/" + subjectID + "/subjectTypes/" + typeID + "/decrementAttendance";
        String uri = "/studies/" + studyID + "/subjects/" + subjectID;

        try {
            restTemplate.exchange(
                    apiUri,
                    HttpMethod.POST,
                    null,
                    Void.class
            );
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        return "redirect:" + uri;
    }

    @PostMapping("/incrementAttendance/{typeID}")
    public String incrementAttendance(
            RedirectAttributes redirectAttrs,
            OAuth2AuthenticationToken authenticationToken,
            @PathVariable String studyID,
            @PathVariable String subjectID,
            @PathVariable String typeID
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String apiUri = apiUrl + "/users/" + user.getUserID() + "/subjects/" + subjectID + "/subjectTypes/" + typeID + "/incrementAttendance";
        String uri = "/studies/" + studyID + "/subjects/" + subjectID;

        try {
            restTemplate.exchange(
                    apiUri,
                    HttpMethod.POST,
                    null,
                    Void.class
            );
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        return "redirect:" + uri;
    }

    @DeleteMapping("/{typeID}")
    public RedirectView deleteType(
            @PathVariable String studyID,
            @PathVariable String subjectID,
            @PathVariable String typeID,
            RedirectAttributes redirectAttrs,
            OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String apiUri = apiUrl + "/users/" + user.getUserID() + "/subjects/" + subjectID + "/subjectTypes/" + typeID;
        String uri = "/studies/" + studyID + "/subjects/" + subjectID;

        SubjectType subjectType = restTemplate.exchange(
                apiUri,
                HttpMethod.GET,
                null,
                SubjectType.class
        ).getBody();
        assert subjectType != null;

        try {
            restTemplate.exchange(
                    apiUri,
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
            String message = "Subject type " + subjectType.getTypeName() + " deleted.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl(uri);
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }
}

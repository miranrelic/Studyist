package com.relic.frontend.controllers;

import com.relic.frontend.models.ApiUser;
import com.relic.frontend.models.Exception;
import com.relic.frontend.models.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Objects;

@Controller
public class HomeController {

    @Value("${api.url}")
    String apiUrl;

    @GetMapping("/")
//    @HxRequest
    public String index(Model model, RedirectAttributes redirectAttrs,
                        OAuth2AuthenticationToken authenticationToken
    ) {
        if (authenticationToken != null) {
            User user = new User(authenticationToken);
            model.addAttribute("user", user);
        }
        return "index";
    }

    @GetMapping("/video")
    public String carlton(
    ) {
        return "video";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/dashboard")
    public String dashboard(Model model, RedirectAttributes redirectAttrs,
                            OAuth2AuthenticationToken authenticationToken
    ) {
        User user = new User(authenticationToken);
        model.addAttribute("user", user);
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<ApiUser> result = restTemplate.exchange(
                    apiUrl + "/users/" + user.getUserID(),
                    HttpMethod.GET,
                    null,
                    ApiUser.class
            );
        } catch (HttpClientErrorException | HttpServerErrorException findError) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<ApiUser> result = restTemplate.exchange(
                        apiUrl + "/users",
                        HttpMethod.POST,
                        new HttpEntity<ApiUser>(new ApiUser(
                                user.getUserID(), user.getFirstName(), user.getLastName(), user.getEmail())),
                        ApiUser.class
                );
            } catch (HttpClientErrorException | HttpServerErrorException createError) {
                redirectAttrs.addFlashAttribute("exception",
                        Objects.requireNonNull(createError.getResponseBodyAs(Exception.class)).getErrorMessage());
                return "redirect:/";
            }
        }

        model.addAttribute("pageTitle", "Dashboard");
        return "dashboard";
    }


    @GetMapping("/account")
    public String account(Model model
    ) {
        return "redirect:http://192.168.0.108:6794/realms/Studyist/account";
    }
}
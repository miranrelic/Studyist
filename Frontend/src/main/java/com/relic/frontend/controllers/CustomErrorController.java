package com.relic.frontend.controllers;

import com.relic.frontend.models.User;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomErrorController implements ErrorController {

    Logger logger = LoggerFactory.getLogger(CustomErrorController.class);

    @RequestMapping("/error")
    public String handleError(Model model, HttpServletRequest request,
                              OAuth2AuthenticationToken authenticationToken) {

        if (authenticationToken != null) {
            User user = new User(authenticationToken);
            model.addAttribute("user", user);
        }

        Object errorCode = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Object errorMessage = request.getAttribute(RequestDispatcher.ERROR_MESSAGE);
        Object errorTrace = request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);

        if (errorCode != null) {
            if (errorCode.equals(405) && authenticationToken == null)
                model.addAttribute("error", "Not authorized");
        }

        logger.error("Error code: " + errorCode);
        logger.error("Error message: " + errorMessage);
        logger.error("Error exception: " + errorTrace);
        return "error";
    }
}

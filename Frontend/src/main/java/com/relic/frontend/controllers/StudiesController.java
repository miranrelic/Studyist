package com.relic.frontend.controllers;

import com.relic.frontend.models.Exception;
import com.relic.frontend.models.PaginatedResponse;
import com.relic.frontend.models.Study;
import com.relic.frontend.models.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Objects;

@PreAuthorize("isAuthenticated()")
@Controller
@RequestMapping("/colleges/{collegeID}/studies")
public class StudiesController {

    @Value("${api.url}")
    String apiUrl;

    @GetMapping
    public String index(Model model, @PathVariable String collegeID,
                        RedirectAttributes redirectAttrs,
                        OAuth2AuthenticationToken authenticationToken
    ) {
        User user = new User(authenticationToken);
        model.addAttribute("user", user);
        String uri = apiUrl + "/users/" + user.getUserID() + "/colleges/" + collegeID + "/studies";

        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<PaginatedResponse<Study>> result = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );
            List<Study> studies = Objects.requireNonNull(result.getBody()).getContent();
            model.addAttribute("collegeID", collegeID);
            model.addAttribute("studies", studies);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/colleges";
        }

        model.addAttribute("pageTitle", "Studies");
        return "studies";
    }


    @GetMapping("/new")
    public String newStudy(Model model, @PathVariable String collegeID) {

        Study study = new Study();
        String uri = "/colleges/" + collegeID + "/studies";

        model.addAttribute("study", study);
        model.addAttribute("uri", uri);
        model.addAttribute("method", "post");

        return "studies_form";
    }

    @PostMapping
    public RedirectView addStudy(@ModelAttribute Study study,
                                 @PathVariable String collegeID,
                                 RedirectAttributes redirectAttrs,
                                 OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = "/colleges/" + collegeID + "/studies";

        try {
            restTemplate.exchange(
                    apiUrl + "/users/" + user.getUserID() + uri,
                    HttpMethod.POST,
                    new HttpEntity<Study>(study),
                    Void.class
            );
            String message = "Study " + study.getStudyName() + " added.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }
        RedirectView rv = new RedirectView();
        rv.setUrl(uri);
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;

    }

    @DeleteMapping("/{studyID}")
    public RedirectView deleteStudy(@PathVariable String collegeID,
                                    @PathVariable String studyID,
                                    RedirectAttributes redirectAttrs,
                                    OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = "/colleges/" + collegeID + "/studies";

        Study study = restTemplate.exchange(
                apiUrl + "/users/" + user.getUserID() + "/studies/" + studyID,
                HttpMethod.GET,
                null,
                Study.class
        ).getBody();
        assert study != null;

        try {
            restTemplate.exchange(
                    apiUrl + "/users/" + user.getUserID() + uri + "/" + studyID,
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
            String message = "Study " + study.getStudyName() + " deleted.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl(uri);
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }

    @GetMapping("/edit/{studyID}")
    public String editStudy(Model model,
                            RedirectAttributes redirectAttrs,
                            @PathVariable String collegeID,
                            @PathVariable String studyID,
                            OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        Study study;
        String uri = "/colleges/" + collegeID + "/studies";

        try {
            String apiUri = apiUrl + "/users/" + user.getUserID() + "/studies/" + studyID;
            study = restTemplate.exchange(
                    apiUri,
                    HttpMethod.GET,
                    null,
                    Study.class
            ).getBody();
            model.addAttribute("study", study);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/professors";
        }

        model.addAttribute("collegeID", collegeID);
        model.addAttribute("uri", uri + "/" + studyID);
        model.addAttribute("method", "put");

        return "studies_form";
    }

    @RequestMapping(value = "/{studyID}", method = {RequestMethod.POST, RequestMethod.PUT})
    public RedirectView updateStudy(@ModelAttribute Study study,
                                    @PathVariable String collegeID,
                                    @PathVariable String studyID,
                                    RedirectAttributes redirectAttrs,
                                    OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = "/colleges/" + collegeID + "/studies";

        try {
            restTemplate.exchange(
                    apiUrl + "/users/" + user.getUserID() + "/studies/" + studyID,
                    HttpMethod.PUT,
                    new HttpEntity<Study>(study),
                    Void.class
            );
            String message = "Study " + study.getStudyName() + " updated.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl(uri);
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }
}

package com.relic.frontend.controllers;

import com.relic.frontend.models.College;
import com.relic.frontend.models.Exception;
import com.relic.frontend.models.PaginatedResponse;
import com.relic.frontend.models.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Objects;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping("/colleges")
public class CollegesController {
    @Value("${api.url}")
    String apiUrl;

    @GetMapping
    public String dashboard(Model model, RedirectAttributes redirectAttrs,
                            OAuth2AuthenticationToken authenticationToken
    ) {
        User user = new User(authenticationToken);
        model.addAttribute("user", user);

        try {
            String uri = apiUrl + "/users/" + user.getUserID() + "/colleges";
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<PaginatedResponse<College>> result = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );
            List<College> colleges = result.getBody().getContent();
            model.addAttribute("colleges", colleges);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/";
        }
        model.addAttribute("pageTitle", "Colleges");
        return "colleges";
    }

    @GetMapping("/new")
    public String newCollege(Model model) {

        College college = new College();
        String uri = "colleges";

        model.addAttribute("college", college);
        model.addAttribute("uri", uri);
        model.addAttribute("method", "post");

        return "college_form";
    }

    @PostMapping
    public String addCollege(@ModelAttribute College college,
                             RedirectAttributes redirectAttrs,
                             OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = apiUrl + "/users/" + user.getUserID() + "/colleges";

        try {
            ResponseEntity<Void> result = restTemplate.exchange(
                    uri,
                    HttpMethod.POST,
                    new HttpEntity<College>(college),
                    Void.class
            );
            String message = "College " + college.getCollegeName() + " added.";
            redirectAttrs.addFlashAttribute("message", message);
            return "redirect:/colleges";
        } catch (HttpClientErrorException | HttpServerErrorException e){
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
            return "redirect:/colleges";
        }

    }

    @DeleteMapping("/{collegeID}")
    public RedirectView deleteCollege(@PathVariable String collegeID,
                                      RedirectAttributes redirectAttrs,
                                      OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = apiUrl + "/users/" + user.getUserID() + "/colleges/" + collegeID;

        College college = restTemplate.exchange(
                uri,
                HttpMethod.GET,
                null,
                College.class
        ).getBody();
        assert college != null;

        try {
            ResponseEntity<Void> result = restTemplate.exchange(
                    uri,
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
            String message = "College " + college.getCollegeName() + " deleted.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl("/colleges");
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }

    @GetMapping("/edit/{collegeID}")
    public String editCollege(Model model,
                              RedirectAttributes redirectAttrs,
                              @PathVariable String collegeID,
                              OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        College college;
        String uri = "/colleges/" + collegeID;

        try {
            String apiUri = apiUrl + "/users/" + user.getUserID() + uri;
            college = restTemplate.exchange(
                    apiUri,
                    HttpMethod.GET,
                    null,
                    College.class
            ).getBody();
            model.addAttribute("college", college);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/colleges";
        }

        model.addAttribute("college", college);
        model.addAttribute("uri", uri);
        model.addAttribute("method", "put");

        return "college_form";
    }

    @RequestMapping(value = "/{collegeID}", method = {RequestMethod.POST, RequestMethod.PUT})
    public RedirectView updateCollege(@ModelAttribute College college,
                                      @PathVariable String collegeID,
                                      RedirectAttributes redirectAttrs,
                                      OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = apiUrl + "/users/" + user.getUserID() + "/colleges/" + collegeID;

        try {
            restTemplate.exchange(
                    uri,
                    HttpMethod.PUT,
                    new HttpEntity<College>(college),
                    Void.class
            );
            String message = "College " + college.getCollegeName() + " updated.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl("/colleges");
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }


}

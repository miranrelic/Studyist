package com.relic.frontend.controllers;

import com.relic.frontend.models.Exception;
import com.relic.frontend.models.PaginatedResponse;
import com.relic.frontend.models.Professor;
import com.relic.frontend.models.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/professors")
public class ProfessorsController {

    @Value("${api.url}")
    String apiUrl;

    @PreAuthorize("isAuthenticated()")
    @GetMapping
    public String index(Model model, RedirectAttributes redirectAttrs,
                        OAuth2AuthenticationToken authenticationToken
    ) {
        User user = new User(authenticationToken);
        model.addAttribute("user", user);

        try {
            String uri = apiUrl + "/users/" + user.getUserID() + "/professors";
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<PaginatedResponse<Professor>> result = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );
            List<Professor> professors = result.getBody().getContent();
            model.addAttribute("professors", professors);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/";
        }
        model.addAttribute("pageTitle", "Professors");
        return "professors";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/new")
    public String newProfessor(Model model) {

        Professor professor = new Professor();
        String submitUri = "/professors";
        String cancelUri = "/professors";

        model.addAttribute("professor", professor);
        model.addAttribute("submitUri", submitUri);
        model.addAttribute("cancelUri", cancelUri);
        model.addAttribute("hx-target", "body");
        model.addAttribute("hx-swap", "innerHTML");
        model.addAttribute("method", "post");

        return "professor_form";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping
    public String addProfessor(@ModelAttribute Professor professor,
                               RedirectAttributes redirectAttrs,
                               OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = apiUrl + "/users/" + user.getUserID() + "/professors";

        try {
            ResponseEntity<Void> result = restTemplate.exchange(
                    uri,
                    HttpMethod.POST,
                    new HttpEntity<Professor>(professor),
                    Void.class
            );
            String message = "Professor " + professor.getFirstName() + " " + professor.getLastName() + " added.";
            redirectAttrs.addFlashAttribute("message", message);
            return "redirect:/professors";
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
            return "redirect:/professors";
        }

    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/{professorID}")
    public RedirectView deleteProfessor(@PathVariable String professorID,
                                        RedirectAttributes redirectAttrs,
                                        OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = apiUrl + "/users/" + user.getUserID() + "/professors/" + professorID;

        Professor professor = restTemplate.exchange(
                uri,
                HttpMethod.GET,
                null,
                Professor.class
        ).getBody();
        assert professor != null;

        try {
            ResponseEntity<Void> result = restTemplate.exchange(
                    uri,
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
            String message = "Professor " + professor.getFirstName() + " " + professor.getLastName() + " deleted.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl("/professors");
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/edit/{professorID}")
    public String editProfessor(Model model,
                                RedirectAttributes redirectAttrs,
                                @PathVariable String professorID,
                                OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        Professor professor;
        String submitUri = "/professors/" + professorID;
        String cancelUri = "/professors/" + professorID;

        try {
            String apiUri = apiUrl + "/users/" + user.getUserID() + "/professors/" + professorID;
            professor = restTemplate.exchange(
                    apiUri,
                    HttpMethod.GET,
                    null,
                    Professor.class
            ).getBody();
            model.addAttribute("professor", professor);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception",
                    Objects.requireNonNull(e.getResponseBodyAs(Exception.class)).getErrorMessage());
            return "redirect:/professors";
        }

        model.addAttribute("professor", professor);
        model.addAttribute("submitUri", submitUri);
        model.addAttribute("cancelUri", cancelUri);
        model.addAttribute("hx-target", "body");
        model.addAttribute("hx-swap", "innerHTML");
        model.addAttribute("method", "put");

        return "professor_form";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{professorID}", method = {RequestMethod.POST, RequestMethod.PUT})
    public RedirectView updateProfessor(@ModelAttribute Professor professor,
                                        @PathVariable String professorID,
                                        RedirectAttributes redirectAttrs,
                                        OAuth2AuthenticationToken authenticationToken
    ) {

        User user = new User(authenticationToken);
        RestTemplate restTemplate = new RestTemplate();
        String uri = apiUrl + "/users/" + user.getUserID() + "/professors/" + professorID;

        try {
            ResponseEntity<Void> result = restTemplate.exchange(
                    uri,
                    HttpMethod.PUT,
                    new HttpEntity<Professor>(professor),
                    Void.class
            );
            String message = "Professor " + professor.getFirstName() + " " + professor.getLastName() + " updated.";
            redirectAttrs.addFlashAttribute("message", message);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            redirectAttrs.addFlashAttribute("exception", e.getResponseBodyAs(Exception.class).getErrorMessage());
        }

        RedirectView rv = new RedirectView();
        rv.setUrl("/professors");
        rv.setStatusCode(HttpStatus.SEE_OTHER);
        return rv;
    }
}

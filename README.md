# Studyist

## ENV variables (example):

### Backend:
```
MONGODB_USER=studyist
MONGODB_PASSWORD=pa$$word
MONGODB_DATABASE=studyistDb
MONGODB_HOST=172.17.0.10
MONGODB_PORT=27017
```
### Frontend
```
KEYCLOAK_CLIENT_ID=my_client
KEYCLOAK_CLIENT_SECRET=2yIC44573yCftqIpP6Zma77QOS0inEBH
KEYCLOAK_URI=https://auth.studyist.eu/realms/Studyist
FRONTEND_URL=https://studyist.eu/
```

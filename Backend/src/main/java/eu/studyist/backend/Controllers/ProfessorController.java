package eu.studyist.backend.Controllers;

import eu.studyist.backend.Services.ProfessorService;
import eu.studyist.backend.entities.Professor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users/{userId}/professors")
public class ProfessorController {

    private final ProfessorService professorService;

    @Autowired
    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }


    @DeleteMapping("/{professorId}")
    public ResponseEntity<Void> deleteProfessor(@PathVariable String userId, @PathVariable String professorId) {
        professorService.deleteProfessor(userId, professorId);
        return ResponseEntity.ok().build();
    }


    @PostMapping
    public ResponseEntity<Void> addProfessor(@PathVariable String userId, @RequestBody Professor professor) {
        professorService.addProfessor(userId, professor);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{professorId}")
    public ResponseEntity<Professor> updateProfessor(@PathVariable String userId, @PathVariable String professorId, @RequestBody Professor professor) {
        return ResponseEntity.ok(professorService.updateProfessor(userId, professorId, professor));
    }

    @GetMapping("/{professorId}")
    public ResponseEntity<Professor> getProfessor(@PathVariable String userId, @PathVariable String professorId) {
        return ResponseEntity.ok(professorService.getProfessor(userId, professorId));
    }

    @GetMapping
    public ResponseEntity<Page<Professor>> getAllProfessors(@PathVariable String userId,
                                                            @PageableDefault(sort = "firstName", direction = Sort.Direction.ASC) Pageable pageable,
                                                            @RequestParam(value = "sortBy", required = false) String sortBy) {
        return ResponseEntity.ok(professorService.getAllProfessors(userId, pageable, sortBy));
    }

}


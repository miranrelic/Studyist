package eu.studyist.backend.Controllers;


import eu.studyist.backend.Services.StudyService;
import eu.studyist.backend.entities.Study;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users/{userId}")
public class StudyController {

    private final StudyService studyService;

    @Autowired
    public StudyController(StudyService studyService) {
        this.studyService = studyService;
    }


    @GetMapping("/studies/{studyId}")
    public ResponseEntity<Study> getStudyById(@PathVariable String userId, @PathVariable String studyId) {
        return ResponseEntity.ok(studyService.getStudy(userId, studyId));
    }


    @DeleteMapping("/colleges/{collegeId}/studies/{studyId}")
    public ResponseEntity<Void> deleteStudy(@PathVariable String userId, @PathVariable String collegeId, @PathVariable String studyId) {
        studyService.deleteStudy(userId, collegeId, studyId);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/studies/{studyId}")
    public ResponseEntity<Study> updateStudy(@PathVariable String userId, @PathVariable String studyId, @RequestBody Study study) {
        return ResponseEntity.ok(studyService.updateStudy(userId, studyId, study));
    }

    @PostMapping("/colleges/{collegeId}/studies")
    public ResponseEntity<Void> addStudyToCollege(@PathVariable String userId, @PathVariable String collegeId, @RequestBody Study study) {
        studyService.createStudy(userId, collegeId, study);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/colleges/{collegeId}/studies")
    public ResponseEntity<Page<Study>> getAllStudies(
            @PathVariable String userId,
            @PathVariable String collegeId,
            @PageableDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable
    ) {
        Page<Study> studies = studyService.getAllStudies(userId, collegeId, pageable);
        if (studies.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(studies, HttpStatus.OK);
    }

}

package eu.studyist.backend.Controllers;

import eu.studyist.backend.Services.SubjectService;
import eu.studyist.backend.entities.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users/{userId}/studies/{studyId}/subjects")
public class SubjectController {


    private final SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @DeleteMapping("/{subjectId}")
    public ResponseEntity<Subject> deleteSubject(@PathVariable String userId, @PathVariable String studyId, @PathVariable String subjectId) {
        return ResponseEntity.ok(subjectService.deleteSubject(userId, studyId, subjectId));
    }


    @PostMapping
    public ResponseEntity<Void> addSubject(@PathVariable String userId, @PathVariable String studyId, @RequestBody Subject subject) {
        subjectService.createSubject(userId, studyId, subject);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{subjectId}")
    public ResponseEntity<Subject> updateSubject(@PathVariable String userId, @PathVariable String subjectId, @RequestBody Subject subject) {
        return ResponseEntity.ok(subjectService.updateSubject(userId, subjectId, subject));
    }

    @GetMapping("/{subjectId}")
    public ResponseEntity<Subject> getSubject(@PathVariable String userId, @PathVariable String subjectId) {
        return ResponseEntity.ok(subjectService.getSubject(userId, subjectId));
    }


    @GetMapping
    public ResponseEntity<Page<Subject>> getAllSubjects(@PathVariable String userId, @PathVariable String studyId,
                                                        @PageableDefault(sort = "subjectName", direction = Sort.Direction.ASC) Pageable pageable) {
        return ResponseEntity.ok(subjectService.getAllSubjects(userId, studyId, pageable));
    }

}

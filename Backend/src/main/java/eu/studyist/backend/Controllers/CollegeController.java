package eu.studyist.backend.Controllers;


import eu.studyist.backend.Services.CollegeService;
import eu.studyist.backend.entities.College;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users/{userId}/colleges")
public class CollegeController {

    private final CollegeService collegeService;

    @Autowired
    public CollegeController(CollegeService collegeService) {
        this.collegeService = collegeService;
    }


    @PostMapping
    public ResponseEntity<Void> createCollege(@PathVariable String userId, @RequestBody College college) {
        collegeService.addCollegeToUser(userId, college);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{collegeId}")
    public ResponseEntity<College> getCollegeById(@PathVariable String userId, @PathVariable String collegeId) {
        return ResponseEntity.ok(collegeService.getCollege(userId, collegeId));
    }


    @GetMapping
    public ResponseEntity<Page<College>> getAllColleges(@PathVariable String userId,
                                                        @RequestParam(value = "sortBy", required = false) String sortBy,
                                                        @PageableDefault(sort = "collegeName", direction = Sort.Direction.ASC) Pageable pageable) {
        return ResponseEntity.ok(collegeService.getAllColleges(userId, pageable, sortBy));
    }

    @DeleteMapping("/{collegeId}")
    public ResponseEntity<Void> deleteCollegeFromUser(@PathVariable String userId, @PathVariable String collegeId) {
        collegeService.deleteCollegeFromUser(userId, collegeId);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{collegeId}")
    public ResponseEntity<College> updateCollegeToUser(@PathVariable String userId, @PathVariable String collegeId, @RequestBody College college) {
        College collegeDb = collegeService.updateCollegeInUser(userId, collegeId, college);
        return ResponseEntity.ok(collegeDb);

    }

}

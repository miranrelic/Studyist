package eu.studyist.backend.Controllers;

import eu.studyist.backend.Services.SubjectTypeService;
import eu.studyist.backend.entities.SubjectType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users/{userId}/subjects/{subjectId}/subjectTypes")

public class SubjectTypeController {

    private final SubjectTypeService SubjectTypeService;

    @Autowired
    public SubjectTypeController(SubjectTypeService SubjectTypeService) {
        this.SubjectTypeService = SubjectTypeService;
    }

    @DeleteMapping("/{subjectTypeId}")
    public ResponseEntity<Void> deleteSubjectType(@PathVariable String userId, @PathVariable String subjectId, @PathVariable String subjectTypeId) {
        SubjectTypeService.deleteSubjectType(userId, subjectId, subjectTypeId);
        return ResponseEntity.ok().build();
    }


    @PostMapping
    public ResponseEntity<String> addSubjectType(@PathVariable String userId, @PathVariable String subjectId, @RequestBody SubjectType subjectTypes) {
        SubjectTypeService.createSubjectType(userId, subjectId, subjectTypes);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{subjectTypeId}")
    public ResponseEntity<SubjectType> updateSubjectType(@PathVariable String userId, @PathVariable String subjectTypeId, @RequestBody SubjectType type) {
        return ResponseEntity.ok(SubjectTypeService.updateType(userId, subjectTypeId, type));
    }

    @GetMapping
    public ResponseEntity<Page<SubjectType>> getAllTypesOfSubject(@PathVariable String userId, @PathVariable String subjectId,
                                                                  @PageableDefault(sort = "subjectType", direction = Sort.Direction.ASC) Pageable pageable) {
        return ResponseEntity.ok(SubjectTypeService.getAllTypesOfSubject(userId, subjectId, pageable));
    }

    @GetMapping("/{subjectTypeId}")
    public ResponseEntity<SubjectType> getSubjectType(@PathVariable String userId, @PathVariable String subjectTypeId) {
        return ResponseEntity.ok(SubjectTypeService.getSubjectType(userId, subjectTypeId));
    }

    @PostMapping("/{subjectTypeId}/incrementAttendance")
    public ResponseEntity<SubjectType> incrementAttendance(@PathVariable String userId, @PathVariable String subjectTypeId) {
        return ResponseEntity.ok(SubjectTypeService.incrementAttendance(userId, subjectTypeId));
    }

    @PostMapping("/{subjectTypesId}/decrementAttendance")
    public ResponseEntity<SubjectType> decrementAttendance(@PathVariable String userId, @PathVariable String subjectTypesId) {
        return ResponseEntity.ok(SubjectTypeService.decrementAttendance(userId, subjectTypesId));
    }

}

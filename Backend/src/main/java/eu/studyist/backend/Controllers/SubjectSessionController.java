package eu.studyist.backend.Controllers;

import eu.studyist.backend.Services.SubjectSessionService;
import eu.studyist.backend.entities.SubjectSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users/{userId}/subjectSession")
public class SubjectSessionController {

    private final SubjectSessionService subjectSessionService;

    @Autowired
    public SubjectSessionController(SubjectSessionService subjectSessionService) {
        this.subjectSessionService = subjectSessionService;
    }


    @DeleteMapping("/{subjectSessionId}")
    public ResponseEntity<Void> deleteSubjectSession(@PathVariable String userId, @PathVariable String subjectSessionId) {
        subjectSessionService.deleteSubjectSession(userId, "aaa", subjectSessionId);
        return ResponseEntity.ok().build();
    }


    @PostMapping
    public ResponseEntity<Void> addSubjectSession(@PathVariable String userId, String subjectTypeId, @RequestBody SubjectSession subjectSession) {
        subjectSessionService.createSubjectSession(userId, subjectTypeId, subjectSession);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{subjectSessionId}")
    public ResponseEntity<SubjectSession> updateSubjectSession(@PathVariable String userId, @PathVariable String subjectSessionId, @RequestBody SubjectSession subjectSession) {
        return ResponseEntity.ok(subjectSessionService.updateSubjectSession(userId, subjectSessionId, subjectSession));
    }

    @GetMapping("/{subjectSessionId}")
    public ResponseEntity<SubjectSession> getSubjectSession(@PathVariable String userId, @PathVariable String subjectSessionId) {
        return ResponseEntity.ok(subjectSessionService.getSubjectSession(userId, subjectSessionId));
    }

    @GetMapping("/{subjectTypeId}")
    public ResponseEntity<List<SubjectSession>> getAllSubjectSessions(@PathVariable String userId, @PathVariable String subjectTypeId) {
        return ResponseEntity.ok(subjectSessionService.getAllSubjectSessions(userId, subjectTypeId));
    }

}
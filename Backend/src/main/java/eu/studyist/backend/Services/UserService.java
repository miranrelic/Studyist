package eu.studyist.backend.Services;

import com.mongodb.MongoWriteException;
import eu.studyist.backend.Repositories.UserRepository;
import eu.studyist.backend.entities.User;
import eu.studyist.backend.exceptionHandler.BadRequestException;
import eu.studyist.backend.exceptionHandler.DataNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(User user) {
        try {
            user.setColleges(new ArrayList<>());
            user.setProfessors(new ArrayList<>());
            return userRepository.insert(user);
        } catch (MongoWriteException e) {
            if (e.getError().getCode() == 11000) {
                // Handle the duplicate key error
                throw new BadRequestException("User with that id already exists");
            } else {
                throw e;
            }
        }
    }

    public User updateUser(String userId, User user) {

        if (user.getFirstName() == null && user.getLastName() == null && user.getEmail() == null) {
            throw new BadRequestException("Body is invalid.");
        }

        User userDb = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (userDb == null) {
            throw new DataNotFoundException("User not found");
        }
        if (Objects.nonNull(user.getFirstName()) && !"".equalsIgnoreCase(user.getFirstName())) {

            userDb.setFirstName(user.getFirstName());
        }

        if (Objects.nonNull(user.getLastName()) && !"".equalsIgnoreCase(user.getLastName())) {

            userDb.setLastName(user.getLastName());
        }

        if (Objects.nonNull(user.getEmail()) && !"".equalsIgnoreCase(user.getEmail())) {
            userDb.setEmail(user.getEmail());
        }

        return userRepository.save(userDb);
    }

    public void deleteUser(String userId) {

        User user = userRepository.findByKeycloakUserId(userId).orElse(null);
        if (user == null) {

            throw new DataNotFoundException("Data Not Found");
        }

        userRepository.deleteByKeycloakUserId(userId);
    }

    public User getUserById(String id) {

        User user = userRepository.findById(id).orElse(null);
        if (user == null) {

            throw new DataNotFoundException("Data Not Found");
        }
        return user;

    }

    public User getUserByKeycloakId(String id) {

        User user = userRepository.findByKeycloakUserId(id).orElse(null);
        if (user == null) {
            throw new DataNotFoundException("User not found with keyCloackUserId " + id);
        }
        return user;

    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

}


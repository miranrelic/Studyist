package eu.studyist.backend.Services;

import eu.studyist.backend.Repositories.UserRepository;
import eu.studyist.backend.entities.*;
import eu.studyist.backend.exceptionHandler.DataNotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
public class SubjectSessionService {

    private final UserRepository userRepository;


    private final SubjectTypeService subjectTypeService;


    @Autowired
    public SubjectSessionService(UserRepository userRepository, SubjectTypeService subjectTypeService) {
        this.subjectTypeService = subjectTypeService;
        this.userRepository = userRepository;
    }


    public void createSubjectSession(String userId, String subjectTypeId, SubjectSession subjectSession) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);
        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);
        }
        SubjectType subjectTypeDb = user.getColleges()
                .stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .map(Subject::getSubjectTypes)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectTypeId().equals(subjectTypeId))
                .findFirst()
                .orElse(null);


        if (subjectTypeDb == null) {
            throw new DataNotFoundException("subjectType not found with subjectTypeId" + subjectTypeId);
        }
        if (subjectSession.getSubjectSessionId() == null) {
            subjectSession.setSubjectSessionId(new ObjectId().toString());
        }


        subjectTypeDb.getSessions().add(subjectSession);

        userRepository.save(user);

    }

    public SubjectSession getSubjectSession(String userId, String subjectSessionId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);
        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);

        }

        SubjectSession subjectSessionDb = user.getColleges()
                .stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .map(Subject::getSubjectTypes)
                .flatMap(Collection::stream)
                .map(SubjectType::getSessions)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectSessionId().equals(subjectSessionId))
                .findFirst()
                .orElse(null);


        if (subjectSessionDb == null) {
            throw new DataNotFoundException("subjectSession not found with subjectSessionId" + subjectSessionId);

        }
        return subjectSessionDb;
    }

    public List<SubjectSession> getAllSubjectSessions(String userId, String subjectTypeId) {
        List<SubjectSession> sessions = subjectTypeService.getSubjectType(userId, subjectTypeId).getSessions();

        if (sessions.isEmpty()) {
            throw new DataNotFoundException("Sessions not found for subjectType with id: " + subjectTypeId);
        }

        return sessions;
    }

    public void deleteSubjectSession(String userId, String subjectTypeId, String subjectSessionId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);

        }

        SubjectSession subjectSession = getSubjectSession(userId, subjectSessionId);


        if (subjectSession == null) {
            throw new DataNotFoundException("subjectSession not found with subjectSessionId" + subjectSessionId);

        }

        Objects.requireNonNull(user.getColleges()
                        .stream()
                        .map(College::getStudies)
                        .flatMap(Collection::stream)
                        .map(Study::getSubjects)
                        .flatMap(Collection::stream)
                        .map(Subject::getSubjectTypes)
                        .flatMap(Collection::stream)
                        .filter(s -> s.getSubjectTypeId().equals(subjectTypeId))
                        .findFirst()
                        .orElse(null))
                .getSessions().remove(subjectSession);
    }

    public SubjectSession updateSubjectSession(String userId, String subjectSessionId, SubjectSession subjectSession) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            System.out.println("Did not found any user!" + userId);
            return null;

        }

        SubjectSession subjectSessiondb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .map(Subject::getSubjectTypes)
                .flatMap(Collection::stream)
                .map(SubjectType::getSessions)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectSessionId().equals(subjectSessionId))
                .findFirst()
                .orElse(null);


        if (subjectSessiondb == null) {
            System.out.println("updateSubjectSession: Did not find any subjectSession with id: " + subjectSessionId);
            return null;
        }
        if (subjectSession.getTime() != null) {
            subjectSessiondb.setTime(subjectSession.getTime());
        }

        if (subjectSession.getDayOfWeek() != null) {
            subjectSessiondb.setDayOfWeek(subjectSession.getDayOfWeek());
        }

        userRepository.save(user);

        return subjectSessiondb;
    }


}

package eu.studyist.backend.Services;


import eu.studyist.backend.Repositories.UserRepository;
import eu.studyist.backend.entities.College;
import eu.studyist.backend.entities.Study;
import eu.studyist.backend.entities.User;
import eu.studyist.backend.exceptionHandler.BadRequestException;
import eu.studyist.backend.exceptionHandler.DataNotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StudyService {

    private final CollegeService collegeService;
    private final UserRepository userRepository;

    @Autowired
    public StudyService(CollegeService collegeService, UserRepository userRepository) {
        this.collegeService = collegeService;
        this.userRepository = userRepository;
    }

    public Study getStudy(String userId, String studyId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("did not found any user!" + userId);
        }

        Study study = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .filter(s -> s.getStudyId().equals(studyId))
                .findFirst()
                .orElse(null);
        if (study == null) {
            throw new DataNotFoundException("did not found any study!" + studyId);

        }
        return study;
    }

    public Page<Study> getAllStudies(String userId, String collegeId, Pageable pageable) {
        College college = collegeService.getCollege(userId, collegeId);
        if (college == null) {
            throw new DataNotFoundException("did not found any study! CollegeId: " + collegeId);
        }

        List<Study> studies = college.getStudies().stream()
                .sorted(Comparator.comparing(Study::getStudyName))
                .collect(Collectors.toList());

        int start = pageable.getPageNumber() * pageable.getPageSize();
        int end = Math.min(start + pageable.getPageSize(), studies.size());
        List<Study> studiesPage = studies.subList(start, end);

        return new PageImpl<>(studiesPage, pageable, studies.size());
    }

    public Study updateStudy(String userId, String studyId, Study study) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);


        if (user == null) {
            throw new DataNotFoundException("Cannot Find User with the given id" + userId);

        }
        Study studyDb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .filter(s -> s.getStudyId().equals(studyId))
                .findFirst()
                .orElse(null);
        if (studyDb == null) {
            throw new DataNotFoundException("Did not find study " + studyId);
        }

        if (study == null) {
            throw new BadRequestException("Empty Body");
        }
        if (study.getStudyName() != null) {
            studyDb.setStudyName(study.getStudyName());
        }

        userRepository.save(user);
        return studyDb;

    }

    public void deleteStudy(String userId, String collegeId, String studyId) {

        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);
        }

        College college = user.getColleges().stream()
                .filter(c -> c.getCollegeId().equals(collegeId))
                .findFirst()
                .orElse(null);

        if (college == null) {
            throw new DataNotFoundException("College not found with collegeId" + collegeId);

        }

        Study studyDb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .filter(s -> s.getStudyId().equals(studyId))
                .findFirst()
                .orElse(null);

        if (studyDb == null) {
            throw new DataNotFoundException("Study not found with studyId" + studyId);
        }

        List<Study> studies = college.getStudies();
        if (studies.isEmpty()) {
            throw new DataNotFoundException("Studies not found for college with collegeId" + collegeId);

        }
        studies.remove(studyDb);

        Objects.requireNonNull(user.getColleges().stream()
                .filter(c -> c.getCollegeId().equals(collegeId))
                .findFirst().orElse(null)).setStudies(studies);

        userRepository.save(user);

    }

    public void createStudy(String userId, String collegeId, Study study) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);
        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);

        }
        System.out.println("addStudyToCollege: Found user " + user);

        // Find the college within the user
        College college = user.getColleges().stream()
                .filter(c -> c.getCollegeId().equals(collegeId))
                .findFirst()
                .orElse(null);
        if (college == null) {
            throw new DataNotFoundException("College not found with collegeId" + collegeId);
        }
        if (study.getStudyId() == null) {
            study.setStudyId(new ObjectId().toString());
        }

        study.setSubjects(new ArrayList<>());
        college.getStudies().add(study);


        userRepository.save(user);

    }


}

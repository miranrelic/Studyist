package eu.studyist.backend.Services;

import eu.studyist.backend.Repositories.CollegeRepositoryCustom;
import eu.studyist.backend.Repositories.UserRepository;
import eu.studyist.backend.entities.College;
import eu.studyist.backend.entities.User;
import eu.studyist.backend.exceptionHandler.BadRequestException;
import eu.studyist.backend.exceptionHandler.DataNotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;


@Service
public class CollegeService {


    private final UserRepository userRepository;
    private final CollegeRepositoryCustom collegeRepository;

    @Autowired
    public CollegeService(UserRepository userRepository, CollegeRepositoryCustom collegeRepository) {

        this.collegeRepository = collegeRepository;
        this.userRepository = userRepository;
    }


    public College getCollege(String userId, String collegeId) {

        College college = collegeRepository.getCollegeFromUser(userId, collegeId);

        if (college == null) {
            throw new DataNotFoundException("College not found!");
        }

        return college;
    }


    public Page<College> getAllColleges(String userId, Pageable pageable, String sortBy) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);
        if (user == null) {
            throw new DataNotFoundException("User with : " + userId + " not found.");
        }

        List<College> colleges = user.getColleges();
        int start = pageable.getPageNumber() * pageable.getPageSize();
        int end = Math.min(start + pageable.getPageSize(), colleges.size());

        List<College> collegePage = colleges.subList(start, end);

        if (Objects.equals(sortBy, "collegeName")) {
            collegePage.sort(Comparator.comparing(c -> c.getCollegeName().toLowerCase()));
        } else if (Objects.equals(sortBy, "address")) {
            collegePage.sort(Comparator.comparing(c -> c.getAddress().toLowerCase()));

        }


        return new PageImpl<>(collegePage, pageable, colleges.size());
    }

    public void addCollegeToUser(String userId, College college) {

        User user = userRepository.findByKeycloakUserId(userId).orElse(null);
        if (user == null) {
            throw new DataNotFoundException("User not found with id: " + userId);
        }
        if (college == null) {
            throw new BadRequestException("Body is empty");
        }
        if (college.getCollegeId() == null) {
            college.setCollegeId(new ObjectId().toString());
        }

        college.setStudies(new ArrayList<>());
        collegeRepository.addCollegeToUser(userId, college);


    }

    public void deleteCollegeFromUser(String userId, String collegeId) {
        College college = getCollege(userId, collegeId);
        if (college == null) {
            throw new DataNotFoundException("College not found! ID:" + collegeId);
        }
        collegeRepository.deleteCollegeFromUser(userId, collegeId);
    }

    public College updateCollegeInUser(String userId, String collegeId, College college) {
        College collegeDb = getCollege(userId, collegeId);

        if (college == null) {
            throw new DataNotFoundException("College not found with id: " + collegeDb);
        }
        if (college.getCollegeName() == null && college.getAddress() == null) {
            throw new BadRequestException("Body is empty");

        }
        collegeRepository.updateCollegeInUser(userId, collegeId, college);

        return collegeRepository.getCollegeFromUser(userId, collegeId);
    }


}

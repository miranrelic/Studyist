package eu.studyist.backend.Services;

import eu.studyist.backend.Repositories.UserRepository;
import eu.studyist.backend.entities.*;
import eu.studyist.backend.exceptionHandler.BadRequestException;
import eu.studyist.backend.exceptionHandler.DataNotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
public class SubjectTypeService {

    private final UserRepository userRepository;


    private final SubjectService subjectService;

    private final ProfessorService professorService;


    @Autowired
    public SubjectTypeService(UserRepository userRepository, SubjectService subjectService, ProfessorService professorService) {
        this.subjectService = subjectService;
        this.userRepository = userRepository;
        this.professorService = professorService;
    }


    public SubjectType getSubjectType(String userId, String subjectTypeId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("updateType: did not found any user!" + userId);

        }
        return user.getColleges()
                .stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .map(Subject::getSubjectTypes)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectTypeId().equals(subjectTypeId))
                .findFirst()
                .orElse(null);

    }


    public Page<SubjectType> getAllTypesOfSubject(String userId, String subjectId, Pageable pageable) {
        Subject subject = subjectService.getSubject(userId, subjectId);
        if (subject == null) {
            throw new DataNotFoundException("did not found any SubjectType! SubjectId: " + subjectId);
        }

        List<SubjectType> subjectTypes = subject.getSubjectTypes();
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), subjectTypes.size());
        List<SubjectType> subjectTypesPage = subjectTypes.subList(start, end);


        return new PageImpl<>(subjectTypesPage, pageable, subjectTypes.size());
    }

    public void createSubjectType(String userId, String subjectId, SubjectType subjectType) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("updateType: did not found any user!" + userId);

        }
        Professor professor = professorService.getProfessor(userId, subjectType.getProfessorId());

        if (professor == null) {
            throw new DataNotFoundException("updateType: did not find professor!" + subjectType.getProfessorId());

        }

        if (subjectType.getSubjectTypeId() == null) {
            subjectType.setSubjectTypeId(new ObjectId().toString());
        }

        subjectType.setProfessorId(subjectType.getProfessorId());
        subjectType.setSessions(new ArrayList<>());


        if (subjectType.getMandatoryAttendancePercentage() > 100 || subjectType.getMandatoryAttendancePercentage() < 0) {
            throw new BadRequestException("Mandatory attendance percentage not valid!");
        }
        subjectType.setMandatoryAttendancePercentage(subjectType.getMandatoryAttendancePercentage() / 100.00);

        double attendanceCountLeft = Math.ceil(subjectType.getTotalSubjectTypeHours() / subjectType.getAttendanceHourValue() * subjectType.getMandatoryAttendancePercentage());

        subjectType.setAttendanceCountLeft(attendanceCountLeft);


        subjectType.setCurrentAttendancePercentage(0.0);
        subjectType.setCurrentAttendanceCount(0.0);

        Objects.requireNonNull(user.getColleges().stream()
                        .map(College::getStudies)
                        .flatMap(Collection::stream)
                        .map(Study::getSubjects)
                        .flatMap(Collection::stream)
                        .filter(s -> s.getSubjectId().equals(subjectId))
                        .findFirst()
                        .orElse(null))
                .getSubjectTypes().add(subjectType);


        userRepository.save(user);


    }

    public SubjectType incrementAttendance(String userId, String subjectTypeId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("updateType: did not found any user!" + userId);

        }

        SubjectType typeOfDb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .map(Subject::getSubjectTypes)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectTypeId().equals(subjectTypeId))
                .findFirst()
                .orElse(null);

        if (typeOfDb == null) {
            throw new DataNotFoundException("did not found any subjectType!" + subjectTypeId);

        }

        typeOfDb.setAttendanceCountLeft(typeOfDb.getAttendanceCountLeft() - 1);
        typeOfDb.setCurrentAttendanceCount(typeOfDb.getCurrentAttendanceCount() + 1);

        typeOfDb.setCurrentAttendancePercentage(typeOfDb.getCurrentAttendanceCount() * typeOfDb.getAttendanceHourValue() / typeOfDb.getTotalSubjectTypeHours());

        userRepository.save(user);
        return typeOfDb;
    }


    public SubjectType decrementAttendance(String userId, String subjectTypeId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("updateType: did not found any user!" + userId);

        }

        SubjectType typeOfDb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .map(Subject::getSubjectTypes)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectTypeId().equals(subjectTypeId))
                .findFirst()
                .orElse(null);

        if (typeOfDb == null) {
            throw new DataNotFoundException("did not found any subjectType!" + subjectTypeId);

        }

        if (typeOfDb.getCurrentAttendanceCount() <= 0) {
            throw new BadRequestException("Cannot decrement current attendance count!");
        }
        typeOfDb.setAttendanceCountLeft(typeOfDb.getAttendanceCountLeft() + 1);
        typeOfDb.setCurrentAttendanceCount(typeOfDb.getCurrentAttendanceCount() - 1);

        typeOfDb.setCurrentAttendancePercentage(typeOfDb.getCurrentAttendanceCount() * typeOfDb.getAttendanceHourValue() / typeOfDb.getTotalSubjectTypeHours());

        userRepository.save(user);
        return typeOfDb;
    }


    public SubjectType updateType(String userId, String subjectTypeId, SubjectType type) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("updateType: did not found any user!" + userId);

        }

        SubjectType typeOfDb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .map(Subject::getSubjectTypes)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectTypeId().equals(subjectTypeId))
                .findFirst()
                .orElse(null);


        if (typeOfDb == null) {
            throw new DataNotFoundException("did not found any subjectType!" + subjectTypeId);

        }

        if (type.getTypeName() == null && type.getClassroom() == null && type.getMandatoryAttendancePercentage() == null
                && type.getTotalSubjectTypeHours() == null && type.getAttendanceHourValue() == null && type.getProfessorId() == null) {
            throw new BadRequestException("Body is malformed");
        }


        if (type.getTypeName() != null) {
            typeOfDb.setTypeName(type.getTypeName());

        }

        if (type.getClassroom() != null) {
            typeOfDb.setClassroom(type.getClassroom());
        }
        if (type.getMandatoryAttendancePercentage() != null) {

            double mandatoryAttendancePercentage = type.getMandatoryAttendancePercentage();
            if (mandatoryAttendancePercentage > 100.00 || mandatoryAttendancePercentage < 0.00) {
                throw new BadRequestException("Mandatory attendance percentage is not valid!");
            }
            mandatoryAttendancePercentage /= 100.00;
            typeOfDb.setMandatoryAttendancePercentage(mandatoryAttendancePercentage);
        }

        if (type.getTotalSubjectTypeHours() != null) {
            typeOfDb.setTotalSubjectTypeHours(type.getTotalSubjectTypeHours());
        }

        if (type.getAttendanceHourValue() != null) {
            typeOfDb.setAttendanceHourValue(type.getAttendanceHourValue());
        }

        if (type.getProfessorId() != null) {

            Professor professor = professorService.getProfessor(userId, type.getProfessorId());

            if (professor == null) {
                throw new DataNotFoundException("updateType: did not find professor!" + type.getProfessorId());
            }
            typeOfDb.setProfessorId(type.getProfessorId());
        }

        double attendanceCountLeft =
                Math.ceil(typeOfDb.getTotalSubjectTypeHours() / typeOfDb.getAttendanceHourValue() *
                        typeOfDb.getMandatoryAttendancePercentage()) - typeOfDb.getCurrentAttendanceCount();
        typeOfDb.setAttendanceCountLeft(attendanceCountLeft);

        userRepository.save(user);

        return typeOfDb;
    }

    public void deleteSubjectType(String userId, String subjectId, String subjectTypeId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + user);

        }
        SubjectType typeDb = getSubjectType(userId, subjectTypeId);

        if (typeDb == null) {
            throw new DataNotFoundException("SubjectType not found with Id" + subjectTypeId);

        }

        Objects.requireNonNull(user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectId().equals(subjectId))
                .findFirst()
                .orElse(null)).getSubjectTypes().remove(typeDb);

        userRepository.save(user);

    }

}

package eu.studyist.backend.Services;

import eu.studyist.backend.Repositories.ProfessorRepository;
import eu.studyist.backend.Repositories.UserRepository;
import eu.studyist.backend.entities.Professor;
import eu.studyist.backend.entities.User;
import eu.studyist.backend.exceptionHandler.BadRequestException;
import eu.studyist.backend.exceptionHandler.DataNotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;


@Service
public class ProfessorService {

    private final UserRepository userRepository;
    private final ProfessorRepository professorRepository;


    @Autowired
    public ProfessorService(UserRepository userRepository, ProfessorRepository professorRepository) {
        this.userRepository = userRepository;
        this.professorRepository = professorRepository;
    }


    public void addProfessor(String userId, Professor professor) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("updateType: did not found any user!" + userId);
        }
        if (professor.getProfessorId() == null) {
            professor.setProfessorId(new ObjectId().toString());
        }
        professorRepository.addProfessorToUser(userId, professor);
    }

    public Professor getProfessor(String userId, String professorId) {

        Professor professor = professorRepository.getProfessorFromUser(userId, professorId);

        if (professor == null) {
            throw new DataNotFoundException("Professor not found!");
        }

        return professor;
    }

    public Page<Professor> getAllProfessors(String userId, Pageable pageable, String sortBy) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);
        if (user == null) {
            throw new DataNotFoundException("User with : " + userId + " not found.");
        }

        List<Professor> professors = user.getProfessors();
        int start = pageable.getPageNumber() * pageable.getPageSize();
        int end = Math.min(start + pageable.getPageSize(), professors.size());

        List<Professor> professorPage = professors.subList(start, end);

        if (Objects.equals(sortBy, "lastName")) {
            professorPage.sort(Comparator.comparing(p -> p.getLastName().toLowerCase()));
        }


        return new PageImpl<>(professorPage, pageable, professors.size());
    }

    public Professor updateProfessor(String userId, String professorId, Professor professor) {
        Professor professorDb = getProfessor(userId, professorId);

        if (professorDb == null) {
            throw new DataNotFoundException("College not found with id: " + professorId);
        }
        if (professor.getFirstName() == null && professor.getLastName() == null && professor.getPhoneNumber() == null && professor.getEmail() == null) {
            throw new BadRequestException("Body is empty");

        }
        professorRepository.updateProfessorInUser(userId, professorId, professor);

        return professorRepository.getProfessorFromUser(userId, professorId);

    }

    public void deleteProfessor(String userId, String professorId) {

        Professor professorDb = professorRepository.getProfessorFromUser(userId, professorId);
        if (professorDb == null) {
            throw new DataNotFoundException("college found! ID:" + professorId);
        }
        professorRepository.deleteProfessorFromUser(userId, professorId);
    }


}

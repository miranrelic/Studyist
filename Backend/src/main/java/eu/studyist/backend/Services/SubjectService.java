package eu.studyist.backend.Services;


import eu.studyist.backend.Repositories.UserRepository;
import eu.studyist.backend.entities.College;
import eu.studyist.backend.entities.Study;
import eu.studyist.backend.entities.Subject;
import eu.studyist.backend.entities.User;
import eu.studyist.backend.exceptionHandler.BadRequestException;
import eu.studyist.backend.exceptionHandler.DataNotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class SubjectService {

    private final StudyService studyService;
    private final UserRepository userRepository;

    @Autowired
    public SubjectService(StudyService studyService, UserRepository userRepository) {
        this.studyService = studyService;
        this.userRepository = userRepository;
    }


    public Subject getSubject(String userId, String subjectId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);

        }
        Subject subjectDb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectId().equals(subjectId))
                .findFirst()
                .orElse(null);

        if (subjectDb == null) {
            throw new DataNotFoundException("Subject not found with subjectId" + subjectId);
        }
        return subjectDb;
    }

    public Subject deleteSubject(String userId, String studyId, String subjectId) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);
        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);

        }
        Subject subject = getSubject(userId, subjectId);

        if (subject == null) {
            throw new DataNotFoundException("Subject not found with subjectId" + subjectId);
        }
        Objects.requireNonNull(user.getColleges().stream()
                        .map(College::getStudies)
                        .flatMap(Collection::stream)
                        .filter(s -> s.getStudyId().equals(studyId))
                        .findFirst()
                        .orElse(null))
                .getSubjects().remove(subject);


        userRepository.save(user);

        return subject;

    }


    public void createSubject(String userId, String studyId, Subject subject) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        subject.setSubjectTypes(new ArrayList<>());

        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);
        }
        Study studyDb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .filter(s -> s.getStudyId().equals(studyId))
                .findFirst()
                .orElse(null);


        if (studyDb == null) {
            throw new DataNotFoundException("Study not found with studyId" + studyId);
        }
        if (subject.getSubjectId() == null) {
            subject.setSubjectId(new ObjectId().toString());
        }

        if (subject.getSubjectName() == null) {
            throw new BadRequestException("Body is empty");
        }

        studyDb.getSubjects().add(subject);
        userRepository.save(user);

    }

    public Subject updateSubject(String userId, String subjectId, Subject subject) {
        User user = userRepository.findByKeycloakUserId(userId).orElse(null);

        if (user == null) {
            throw new DataNotFoundException("User not found with userId" + userId);
        }

        Subject subjectDb = user.getColleges().stream()
                .map(College::getStudies)
                .flatMap(Collection::stream)
                .map(Study::getSubjects)
                .flatMap(Collection::stream)
                .filter(s -> s.getSubjectId().equals(subjectId))
                .findFirst()
                .orElse(null);

        if (subjectDb == null) {
            throw new DataNotFoundException("Subject not found with subjectId" + subjectId);
        }
        if (subject.getSubjectName() != null) {
            subjectDb.setSubjectName(subject.getSubjectName());
        } else {
            throw new BadRequestException("Body is empty");
        }
        userRepository.save(user);


        return subjectDb;
    }


    public Page<Subject> getAllSubjects(String userId, String studyId, Pageable pageable) {
        List<Subject> subjects = studyService.getStudy(userId, studyId).getSubjects().stream()
                .sorted(Comparator.comparing(Subject::getSubjectName))
                .toList();


        int start = pageable.getPageNumber() * pageable.getPageSize();
        int end = Math.min(start + pageable.getPageSize(), subjects.size());
        List<Subject> pageOfSubjects = subjects.subList(start, end);

        return new PageImpl<>(pageOfSubjects, pageable, subjects.size());
    }

}

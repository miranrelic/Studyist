package eu.studyist.backend.Repositories;

import com.mongodb.client.result.UpdateResult;
import eu.studyist.backend.entities.Professor;
import eu.studyist.backend.entities.User;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class ProfessorRepositoryImpl implements ProfessorRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    public ProfessorRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    @Override
    public Professor getProfessorFromUser(String userId, String professorId) {

        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("keycloakUserId").is(userId)),
                Aggregation.unwind("professors"),
                Aggregation.match(Criteria.where("professors.professorId").is(professorId)),
                Aggregation.project().and("professors.professorId").as("professorId")
                        .and("professors.firstName").as("firstName")
                        .and("professors.lastName").as("lastName")
                        .and("professors.email").as("email")
                        .and("professors.phoneNumber").as("phoneNumber")
        );

        AggregationResults<Professor> result = mongoTemplate.aggregate(aggregation, "users", Professor.class);
        return result.getUniqueMappedResult();
    }


    @Override
    public void addProfessorToUser(String userId, Professor professor) {

        Query query = new Query(Criteria.where("keycloakUserId").is(userId));
        Update update = new Update().push("professors", professor);
        UpdateResult result = mongoTemplate.updateFirst(query, update, User.class);
    }

    @Override
    public void updateProfessorInUser(String userId, String professorId, Professor professor) {

        Query query = new Query(Criteria.where("keycloakUserId").is(userId).and("professors.professorId").is(professorId));

        Update update = new Update();
        if (professor.getFirstName() != null) {
            update.set("professors.$.firstName", professor.getFirstName());
        }
        if (professor.getLastName() != null) {
            update.set("professors.$.lastName", professor.getLastName());
        }

        if (professor.getEmail() != null) {
            update.set("professors.$.email", professor.getEmail());
        }

        if (professor.getPhoneNumber() != null) {
            update.set("professors.$.phoneNumber", professor.getPhoneNumber());
        }


        mongoTemplate.updateFirst(query, update, User.class);

    }

    @Override
    public void deleteProfessorFromUser(String userId, String professorId) {

        Query query = new Query(Criteria.where("keycloakUserId").is(userId));
        Update update = new Update().pull("professors", Query.query(Criteria.where("professorId").is(professorId)));
        mongoTemplate.updateFirst(query, update, User.class);

    }
}

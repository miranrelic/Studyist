package eu.studyist.backend.Repositories;

import com.mongodb.client.result.UpdateResult;
import eu.studyist.backend.entities.College;
import eu.studyist.backend.entities.User;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class CollegeRepositoryImpl implements CollegeRepositoryCustom {


    private final MongoTemplate mongoTemplate;

    public CollegeRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void addCollegeToUser(String userId, College college) {

        Query query = new Query(Criteria.where("keycloakUserId").is(userId));
        Update update = new Update().push("colleges", college);
        System.out.println("addCollegeToUser: userId = " + userId + ", college = " + college);
        UpdateResult result = mongoTemplate.updateFirst(query, update, User.class);
        System.out.println("addCollegeToUser: result = " + result);

    }

    @Override
    public void updateCollegeInUser(String userId, String collegeId, College college) {
        Query query = new Query(Criteria.where("keycloakUserId").is(userId).and("colleges.collegeId").is(collegeId));

        Update update = new Update();
        if (college.getCollegeName() != null) {
            update.set("colleges.$.collegeName", college.getCollegeName());
        }
        if (college.getAddress() != null) {
            update.set("colleges.$.address", college.getAddress());
        }


        mongoTemplate.updateFirst(query, update, User.class);

    }

    @Override
    public void deleteCollegeFromUser(String userId, String collegeId) {
        Query query = new Query(Criteria.where("keycloakUserId").is(userId));
        Update update = new Update().pull("colleges", Query.query(Criteria.where("collegeId").is(collegeId)));
        mongoTemplate.updateFirst(query, update, User.class);
    }

    @Override
    public College getCollegeFromUser(String userId, String collegeId) {

        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("keycloakUserId").is(userId)),
                Aggregation.unwind("colleges"),
                Aggregation.match(Criteria.where("colleges.collegeId").is(collegeId)),
                Aggregation.project().and("colleges.collegeId").as("collegeId")
                        .and("colleges.collegeName").as("collegeName")
                        .and("colleges.address").as("address")
                        .and("colleges.studies").as("studies")
        );

        AggregationResults<College> college = mongoTemplate.aggregate(aggregation, "users", College.class);
        return college.getUniqueMappedResult();
    }
}

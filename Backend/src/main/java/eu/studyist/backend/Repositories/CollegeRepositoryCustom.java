package eu.studyist.backend.Repositories;

import eu.studyist.backend.entities.College;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CollegeRepositoryCustom {

    void addCollegeToUser(String userId, College college);

    void updateCollegeInUser(String userId, String collegeId, College college);

    void deleteCollegeFromUser(String userId, String collegeId);

    College getCollegeFromUser(String userId, String collegeId);
}

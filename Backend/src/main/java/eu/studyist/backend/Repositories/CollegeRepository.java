package eu.studyist.backend.Repositories;

import eu.studyist.backend.entities.College;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CollegeRepository extends MongoRepository<College, String>, CollegeRepositoryCustom {
}

package eu.studyist.backend.Repositories;

import eu.studyist.backend.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String>, UserRepositoryCustom {


    User getUserByKeycloakUserId(String keycloakUserId);

    void deleteByKeycloakUserId(String keycloakUserId);

    Optional<User> findByKeycloakUserId(String keycloakUserId);

}
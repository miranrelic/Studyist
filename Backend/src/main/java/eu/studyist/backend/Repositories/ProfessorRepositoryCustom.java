package eu.studyist.backend.Repositories;

import eu.studyist.backend.entities.Professor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ProfessorRepositoryCustom {

    Professor getProfessorFromUser(String userId, String professorId);

    void addProfessorToUser(String userId, Professor professor);

    void updateProfessorInUser(String userId, String professorId, Professor professor);

    void deleteProfessorFromUser(String userId, String professorId);
}

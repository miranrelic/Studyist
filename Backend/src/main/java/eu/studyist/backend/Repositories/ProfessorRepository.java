package eu.studyist.backend.Repositories;


import eu.studyist.backend.entities.Professor;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProfessorRepository extends MongoRepository<Professor, String>, ProfessorRepositoryCustom {
}

package eu.studyist.backend.entities;

import lombok.Data;
import org.bson.types.ObjectId;

import java.util.List;

@Data
public class College {


    private String collegeId;
    private String collegeName;
    private String address;
    private List<Study> studies;

    public College() {
        collegeId = new ObjectId().toString();
    }


}

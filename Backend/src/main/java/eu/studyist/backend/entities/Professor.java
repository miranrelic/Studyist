package eu.studyist.backend.entities;

import lombok.Data;
import org.bson.types.ObjectId;


@Data
public class Professor {


    private String professorId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;

    public Professor() {
        professorId = new ObjectId().toString();
    }
}
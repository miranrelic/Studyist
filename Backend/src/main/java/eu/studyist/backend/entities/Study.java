package eu.studyist.backend.entities;


import lombok.Data;
import org.bson.types.ObjectId;

import java.util.List;

@Data
public class Study {


    private String studyId;
    private String studyName;
    private List<Subject> subjects;

    public Study() {
        studyId = new ObjectId().toString();
    }
}
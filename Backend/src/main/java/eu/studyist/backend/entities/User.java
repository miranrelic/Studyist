package eu.studyist.backend.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "users")
public class User {

    //For user, since it is a Document, id will be automatically generated.
    // that is not the case for the sub documents, hence the need for the Constructor
    // inside other entities which will generate the id.
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private String userId;


    @NotNull(message = "KeycloakId cannot be null...")
    @Indexed(unique = true)
    private String keycloakUserId;

    private String firstName;
    private String lastName;
    private String email;
    private List<College> colleges;
    private List<Professor> professors;

}

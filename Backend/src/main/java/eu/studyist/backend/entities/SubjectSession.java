package eu.studyist.backend.entities;

import lombok.Data;
import org.bson.types.ObjectId;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class SubjectSession {
    LocalDate dayOfWeek;
    LocalTime time;
    private String subjectSessionId;

    public SubjectSession() {
        subjectSessionId = new ObjectId().toString();
    }

}

package eu.studyist.backend.entities;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.bson.types.ObjectId;

import java.util.List;

@Data
public class SubjectType {


    private String subjectTypeId;
    private String typeName;
    private String professorId;
    private String classroom;
    private List<SubjectSession> sessions;
    //Kalkulira se
    private Double currentAttendancePercentage;
    //Kalkulira se
    private Double attendanceCountLeft;
    //MandatoryAttendance
    @NotNull(message = "mandatoryAttendancePercentage cannot be null...")
    private Double mandatoryAttendancePercentage;
    //UkupanBrojSati
    @NotNull(message = "totalSubjectTypeHours cannot be null...")
    private Double totalSubjectTypeHours;
    //VrijednostJednogDolaska
    @NotNull(message = "attendanceHourValue cannot be null...")
    private Double attendanceHourValue;
    //+1 ili -1
    private Double currentAttendanceCount;


    public SubjectType() {
        subjectTypeId = new ObjectId().toString();
    }
}

package eu.studyist.backend.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

@Data
public class Classroom {


    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private String id;
    private String classroomNumber;

    public Classroom() {
        id = new ObjectId().toString();
    }
}
package eu.studyist.backend.entities;

import lombok.Data;
import org.bson.types.ObjectId;

import java.util.List;

@Data
public class Subject {


    private String subjectId;
    private String subjectName;

    private List<SubjectType> subjectTypes;

    public Subject() {
        subjectId = new ObjectId().toString();
    }

}

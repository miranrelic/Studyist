package eu.studyist.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})

@EnableMongoRepositories(basePackages = "eu.studyist.backend.Repositories")
public class BackendResourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendResourcesApplication.class, args);
    }

}

#!/bin/bash

java -jar backend.jar &
java -jar frontend.jar &

wait -n

exit $?
FROM amazoncorretto:17-alpine-jdk
WORKDIR /app

COPY Backend/target/*.jar ./backend.jar
COPY Frontend/target/*.jar ./frontend.jar
COPY ./startStudyist.sh ./startStudyist.sh

RUN chmod +x ./startStudyist.sh

EXPOSE 8080

CMD /bin/sh startStudyist.sh
